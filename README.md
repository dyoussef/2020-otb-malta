## Virtual Workshop: Remote sensing from space and applications

### Malta Council for Science and Technology, Villa Bighi, Kalkara

#### Wednesday 14th October 2020 (Day 2)

* 09.00 - 10.30	 	[Introduction to the Orfeo Toolbox + Install Party](./Slides/sections/01-intro-en.tex)
* 10.30 - 11.00		Coffee Break
* 11:00 - 12:30		[A brief tour of Monteverdi + OTB-Applications](./Slides/sections/02-monteverdi-en.tex)
* 12.30 - 13.30 	Lunch
* 13.30 - 15.00 	OTB Processing in Python (Part 1: LargeScaleSegmentation)
* 15:00 - 15:30		Coffee Break
* 15:30 - 17:00		OTB Processing in Python (Part 2: Classification)

## Installation Instructions: Linux

In order to install the Orfeo Toolbox you need to install the prerequisites. You can do this using the following commands
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python3-dev
sudo apt-get install python3 numpy
sudo apt install cmake
sudo apt-get install build-essential
sudo apt-get install libgl1-mesa-dev
```
Download the 64-bit Linux installation of the Orfeo Toolbox
```
mkdir -p install && cd install
wget https://www.orfeo-toolbox.org/packages/OTB-7.2.0-Linux64.run
```

This package is a self-extractable archive. You may uncompress it with a double-click on the file, or from the command line as follows:

```
chmod +x OTB-7.2.0-Linux64.run
./OTB-7.2.0-Linux64.run
cd OTB-7.2.0-Linux64/
ctest -S share/otb/swig/build_wrapping.cmake -VV
```

To activate the python virtual environment, use the following commands
```
source ./otbenv.profile
```
Finally, use the following script to test that the Orfeo Toolbox is working

```
python3
>>> import otbApplication
```
You are now ready to start using the Orfeo Toolbox.
