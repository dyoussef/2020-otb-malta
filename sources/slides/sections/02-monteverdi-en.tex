\begin{frame}{A brief tour of Monteverdi + OTB-Applications}
  \begin{block}{Pre-requisites}
    \begin{itemize}
    \item Software installed (Monteverdi and Orfeo ToolBox)
    \item Data downloaded
    \end{itemize}
  \end{block}

  \begin{block}{Objectives}
    \begin{itemize}
    \item Know how to look for an application in the list of available applications
    \item Know how to set application parameters
    \item Know where the documentation of an application is
    \item Know how to use several classical applications.
    \end{itemize}
  \end{block}    
  \begin{block}{Outline}
    We will learn how to use Monteverdi and the Orfeo Toolbox applications. Pléiades images extracts including messages encoded by steganography are available. You will first look at the image to try to detect the message, then try to use suggested applications to reveal this hidden message for each image.
  \end{block}
\end{frame}

\begin{frame}{Monteverdi overview}
  \begin{block}{Image displaying}
    \begin{itemize}
      \item Open images (File $\rightarrow$ Open image(s))
      \item Scroll view with mouse drag (+CTRL freeze rendering), Zoom with mouse wheel
    \end{itemize}
  \end{block}
  \begin{block}{Widgets}
    \begin{itemize}
    \item Quicklook, Histogram, Color Setup, Color dynamics, Layer stack
    \item OTB Apps (View $\rightarrow$ OTB-Applications browser)
    \end{itemize}
  \end{block}

  \begin{block}{Nice keyboard shortcuts (Help $\rightarrow$ Keymap)}
    \begin{itemize}
    \item Layer FX :
      \begin{itemize}
      \item Chessboard: C
      \item Local contrast: D, Local translucency: T, Spectral angle: S
      \item Swipe horizontal: H,  Swipe Vertical: V
      \item Normal: N
      \end{itemize}
    \item Histogram control:
      \begin{itemize}
      \item Shift dynamics range towards low/high: CTRL + ALT + scroll
      \item Scale dynamics range around it's mean value CTRL + ALT + SHIFT + scroll
        \end{itemize}
    \end{itemize}
  \end{block}
  
\end{frame}

\begin{frame}{OTB-Applications overview}
  \begin{block}{Choose an application:}
    \begin{itemize}
    \item Display help from an app (orthorectification for example)
    \item Browse parameters
      \begin{itemize}
      \item \emph{parameter groups}
      \item \emph{mandatory parameters}
      \item Dynamic parameters dependency
      \item Default value
      \item Type of output images (default is float)
      \end{itemize}
    \end{itemize}
  \end{block}

\begin{block}{Another example: BandMath}
  \begin{itemize}
  \item Raster calculator using the \href{https://beltoforion.de/article.php?a=muparser}{MuParser library}
  \item Input is a list of images (\emph{-il} parameter)
  \item Mathematical expressions syntax:
    \begin{itemize}
    \item imXbY:
      \begin{itemize}
      \item X: index of image in list (from 1 to N)
      \item Y: index of image band (from 1 to N)
      \end{itemize}
    \end{itemize}
  \item Examples: -exp "(im1b4 - im1b1) / (im1b4 - im1b1)"
  \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Exercise: Message 1}
  In \verb~image1.tif~ image, a sentence has been encoded in a high
  signal area, using a pixel value which is outside the possible
  range for Pléiades (reminder: Pléiades images are encoded with
  unsigned 12 bits integers).

  Use the \alert{BandMath} application to detect those out of range
  values and thus reveal the message.
\end{frame}

\begin{frame}{Exercise: Message 2}
  In \verb~image2.tif~ image, a sentence has been encoded within a
  homogeneous, low signal area. Modified pixels can not be seen
  with human eyes, but could be revealed by computing the image
  gradient, or some edge detection.
  
  Use the \alert{EdgeExtraction} application to reveal the message.
\end{frame}

\begin{frame}{Exercise: Message 3}
  In \verb~image3.tif~ image, a sentence has been encoded by slightly
  modifying the pixel values in the red and near infra-red
  bands. This modification can not be seen with human eyes, but
  could be revealed by computing an NDVI radiometric index.
  
  Use the \alert{RadiometricIndices} application to reveal the message.
  
  You can also use the \alert{BandMath} application to compute the NDVI
  index using the following formula:
  
  \begin{center}
    $NDVI = \frac{NIR-RED}{NIR+RED}$
  \end{center}
  
  Reminder: For Pléiades images the red band is the first one, and the NIR
  band is the last one.
\end{frame}

\begin{frame}{Exercise: Message 4}
  In \verb~image4.tif~, a message has been hidden in the 2 least
  significant bits of the image. This modification can not be
  detected by human eyes, but could be revealed by isolating the
  values of those 2 bits.
  
  Use the \alert{BandMath} application to isolate the 2 least significant
  bits in the image (encoded on 12 bits), to reveal the message.
  
  \alert{Note:} The rint() function allows to round a floating point
  value to nearest integer in \alert{BandMath} application.
\end{frame}

\begin{frame}{Exercise: Message 5}
  In image \verb~image5.tif~, a message has been dissimulated by
  locally slightly modifying the image noise. It could be revealed
  by a transform that isolates noise.
  
  Use the \alert{DimensionalityReduction} application to isolate the
  image noise and reveal the message.
  
  You can also try to figure out other techniques using the
  applications to highlight this local modification of the image noise.
\end{frame}

\begin{frame}{Exercise: Message 6}
  In \verb~image6.tif~ image, a message has been hidden by locally
  using a gray-level morphological operation (opening with radius=1).
  It could be revealed by using the idempotent property of this
  transform. A function $f$ is said idempotent if:
  
  \begin{center}
    $f(f(x))=f(x)$
  \end{center}

  Use the \alert{GrayscaleMorphologicalOperation} and \alert{BandMath}
  applications to reveal the message by using this idempotent property.
\end{frame}

\begin{frame}{Going further}
  What messages were you able to detect by analyzing the image with
  Monteverdi? What messages were impossible to detect?
  
  Can you imagine another process to encode hidden messages in
  images? An image (\verb~image.tif~) and a message (\verb~message.tif~) are
  provided in the \verb~Data/stegano/diy~ folder for you to try.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution: Message 1}
  To reveal the first message, one can observe that a Pléiades image
  is encoded using 12 bits, and that there should not be any pixel
  value higher than $2^{12}-1=4095$. We will therefore use the
  \alert{BandMath} application to threshold pixels higher than this value:
  
  \begin{lstlisting}[style=shared, language=bash]
    $ otbcli_BandMath -il image1.tif -out decoded1.tif uint8 -exp "im1b1>4095?255:0"
  \end{lstlisting}
  
  The encoded text will appear in white on a black background.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution: Message 2}
  To decode the second message we will compute the image gradient
  using the \alert{EdgeExtraction} application:

  \begin{lstlisting}[style=shared, language=bash]
    $ otbcli_EdgeExtraction -in image2.tif    \
    -filter gradient    \
    -out decoded2.tif
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution: Message 3}
  To decode the third message, one can compute a vegetation index
  such as the NDVI using the \alert{RadiometricIndices} application:

  \begin{lstlisting}[style=shared, language=bash]
    $ otbcli_RadiometricIndices -in image3.tif      \
    -channels.red 1       \
    -channels.nir 4       \
    -list Vegetation:NDVI \
    -out decoded3.tif
  \end{lstlisting}

  One can also compute the NDVI using the \alert{BandMath} application:

  \begin{lstlisting}[style=shared, language=bash]
    $ otbcli_BandMath -il image3.tif                    \
    -out decoded3.tif                 \
    -exp "(im1b4-im1b1)/(im1b4+im1b1)"
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution: Message 4}
  To reveal the 4th message, we are going to isolate the 2 least
  significant bits using the \alert{BandMath} application:

  \begin{lstlisting}[style=shared, language=bash]
    $ otbcli_BandMath -il image4.tif               \
    -out decoded4.tif            \
    -exp "im1b1-4*rint(im1b1/4)"
  \end{lstlisting}

  The $4*rint(im1b1/4)$ expression does not contain the 2 least
  significant bits, and the difference with original image thus
  reveals the message. 
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution: Message 5}
  To reveal this 5th message, we are going to do a principal
  component analysis using the \alert{DimensionalityReduction}
  application, and extract the last band, where the image noise is
  condensed, using the \alert{ExtractROI}
  application.

  \begin{lstlisting}[style=shared, language=bash]
    $ otbcli_DimensionalityReduction -in image5.tif  -out pca6.tif  -method pca
    $ otbcli_ExtractROI -in pca6.tif -out decoded6.tif -cl Channel4
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution: Message 6}
  To reveal the 6th message, we are going to use the idempotent
  property. If the message has been encoded using an idempotent
  transform, then $f(message)=message$, and therefore
  $f(message)-message=0$, while outside of the image we will see
  $f(image)$.

  \begin{lstlisting}[style=shared, language=bash, basicstyle=\footnotesize]
    $ otbcli_GrayScaleMorphologicalOperation -in image6.tif  -out ouverture6.tif \
                                             -structype.ball.xradius 1 \
                                             -structype.ball.yradius 1 \
                                             -filter opening
    $ otbcli_BandMath -il image6.tif ouverture6.tif -out decoded6.tif \
                                             -exp "(im2b1-im1b1)"
  \end{lstlisting}
\end{frame}
